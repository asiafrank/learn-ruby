#!/usr/bin/ruby
# coding: utf-8
# Example of excuting command by ruby
def cmdline(command)
  puts "#{command}"
  if !system(command)
    raise "Error: #{command} failed!"
  end
end
if ARGV.length < 2
  raise "Error: autodeploy project application"
end
index = 1
while (index < ARGV.length)
  cmdline("release #{ARGV[0]} #{ARGV[index]}")
  cmdline("deploy #{ARGV[0]} #{ARGV[index]}")
  index = index + 1
end
