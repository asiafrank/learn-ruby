#!/usr/bin/env ruby
require 'erb'

hash = { key1: "val1", key2: "val2" }
string = ""
hash.each { |k,v| string << "#{k} is #{v}\n" }
# solution2
# hash.each { |k,v| string << "#{k} is #{v}\n" }

# solution3
# hash.each { |k,v| string << "#{k} is #{v}\n" }

puts string
# key1 is val1
# key2 is val2

# If your data structure is an array, or easily transformed into an array, it’s usually more
# efficient to use Array#join
puts hash.keys.join("\n") + "\n"
            
data = ["1", "2", "3"]
s = ""
data.each { |x| s << x << " and a " }
puts s
puts data.join(" and a ")

s2 = ""
data.each_with_index { |x, i| s2 << x; s2 << "|" if i < data.length-1 }
puts s2

# Substituting Variables into Strings
number = 5
puts "The number is #{number}."
puts "The number is #{5}."
puts "The number after #{number} is #{number.next}."
puts "The number prior #{number} is #{number-1}."
puts "We're ##{number}!"

# it shows the power feature but you should never use it.
puts %{Here is #{class InstantClass
                   def bar
                     "some text"
                   end
                 end
InstantClass.new.bar
}.}

name = "Mr. Lorum"
email = <<END
Dear #{name},

Unforunately web cannot process your insurance claim at this time.
This is because we are a bakery, not an insurance company.

Signed,
 Nil, Null, and None
 Bakers to Her Majesty the Singleton
END
puts email

str = <<end_of_poem
There once was aman from Peru
Whose limericks stopped on line two
end_of_poem
puts str
puts "\n"

template = 'Oceania has always been at war with %s. %s'
puts template
puts template % ['Eurasia', ':)']
puts template % ['Eastasia', ':)']
puts "\n"

puts 'To 2 decimal places: %.2f' % Math::PI
puts 'Zero-padded: %.5d' % Math::PI
puts "\n"

template2 = ERB.new %q{Chunky <%= food %>!}
food = "bacon"
template2.result(binding)
puts template2.result
food = "peanut butter"
template2.result(binding)
puts template2.result
puts "\n"

template3 = %q{
<% if problems.empty? %>
Looks like your code is clean!
<% else %>
I found the following possible problems with your code:
<% problems.each do |problem, line| %>
* <%= problem %> on line <%= line %>
<% end %>
<% end %>}.gsub(/^\s+/, '')

puts template3
puts "\n"
template3 = ERB.new(template3, nil, '<>')
problems = [
             ["Use of is_a? instead of duck typing", 23],
             ["eval() is usually dangerous", 44]
]
puts template3.run(binding)
# I found the following possible problems with your code:
# * Use of is_a? instead of duck typing on line 23
# * eval() is usually dangerous on line 44
problems = []
puts template3.run(binding)
# Looks like your code is clean!

puts "\n"
class String
  def substitute(binding=TOPLEVEL_BINDING)
    eval(%{"#{self}"}, binding)
  end
end
template4 = %q{Chunky #{food}!} # => "Chunky \#{food}!"
food = 'bacon'
puts template4.substitute(binding) # => "Chunky bacon!"
food = 'peanut butter'
puts template4.substitute(binding) # => "Chunky peanut butter!"

str1 = ".sdrawkcab si gnirts sihT"
puts str1.reverse
puts str1
puts "\n"

puts str1.reverse!
puts str1
puts "\n"

str2 = "order. wrong the in are words These"
puts str2.split(/(\s+)/).reverse!.join('')
puts str2.split(/\b/).reverse!.join('')

puts "Three little words".split(/\s+/) # => ["Three", "little", "words"]
puts "Three little words".split(/(\s+)/) # => ["Three", " ", "little", " ", "words"]

octal = "\000\001\010\020"
octal.each_byte { |x| puts x }
# 0
# 1
# 8
# 16

hexadecimal = "\x00\x01\x10\x20"
hexadecimal.each_byte { |x| puts x }
# 0
# 1
# 16
# 32

puts "\a" == "\x07" # => true # ASCII 0x07 = BEL (Sound system bell)
puts "\b" == "\x08" # => true # ASCII 0x08 = BS (Backspace)
puts "\e" == "\x1b" # => true # ASCII 0x1B = ESC (Escape)
puts "\f" == "\x0c" # => true # ASCII 0x0C = FF (Form feed)
puts "\n" == "\x0a" # => true # ASCII 0x0A = LF (Newline/line feed)
puts "\r" == "\x0d" # => true # ASCII 0x0D = CR (Carriage return)
puts "\t" == "\x09" # => true # ASCII 0x09 = HT (Tab/horizontal tab)
puts "\v" == "\x0b" # => true # ASCII 0x0B = VT (Vertical tab)
