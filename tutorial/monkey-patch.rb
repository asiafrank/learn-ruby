module MyMonkeyPatches
  refine String do
    def length
      30
    end
  end
end

class TestMyMonkey
  using MyMonkeyPatches
  def string_length(string)
    string.length
  end
end

#string = "foobar"
#string.length
#TestMyMonkey.new.string_length(string)
#string.length
