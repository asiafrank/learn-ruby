#!/usr/bin/env ruby
patient1 = {:ruby => "red"}
patient2 = {:ruby => "programming"}

patient1.each_key {|key| puts key.object_id.to_s}
patient2.each_key {|key| puts key.object_id.to_s}

patient1.each_key {|key| puts key.to_s}
patient2.each_key {|key| puts key.to_s}

# Result: symbol is an immutable object and refer to the same object in memory
